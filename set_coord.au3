#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=centre.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Fileversion=2.0.1.0
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

;===============================================================================
;
; Description:      ���������� ��������� ��������� �������
; Name              Set coord
; Version:          2.0.1.0
; Data              24.12.14
; Requirement(s):   Autoit 3.3.8.1
; Author(s):        Vint
;
;===============================================================================

#Region    ************ Includes ************
#include <WindowsConstants.au3>
#include <WinAPIEx.au3>
#EndRegion ************ Includes ************

HotKeySet("{F1}", "Pause")
HotKeySet("{F2}", "Quit")

Global $draw = True, $stage = 0, $Select, $Lab, $x1, $y1, $x2, $y2
Global $trans = 150        ; ������������ 192
Global $color1 = 0x0000FF   ; �����������
Global $color2 = 0xFF0000   ; ���������
$sPath_ini = @ScriptDir & "\coord.ini"

$pos = MouseGetPos() ; �������� ������� ���������� ��������� ����

$Horz = GUICreate("", @DesktopWidth, 1, 0, $pos[1], $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
GUISetBkColor($color1)
WinSetTrans($Horz, '', $trans)
GUISetState(@SW_SHOWNOACTIVATE)

$Vert = GUICreate("", 1, @DesktopHeight, $pos[0], 0, $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
GUISetBkColor($color1)
WinSetTrans($Vert, '', $trans)
GUISetState(@SW_SHOWNOACTIVATE)

$Select = GUICreate("", 1, 1, $x1, $y1, $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
GUISetBkColor($color2)
WinSetTrans($Select, '', 50)
;~ $Lab = GUICtrlCreateLabel('', $x1+1, $y1+1, 0, 0) ; ���
;~ GUICtrlSetBkColor(-1, 0xFF0000)
;~ WinSetTrans($Lab, '', 50)
GUISetState(@SW_HIDE, $Select)

; �����
Do
	$pos = MouseGetPos() ; �������� ������� ���������� ��������� ����
	WinMove($Horz, '', 0, $pos[1])  ;Default
	WinMove($Vert, '', $pos[0], 0)

	;~ 	If $draw Then
	;~ 		WinSetOnTop($Horz, "", 1)
	;~ 		WinSetOnTop($Vert, "", 1)
	;~ 	EndIf

	If $stage = 1 Then
		WinMove($Select, '', $x1, $y1, $pos[0]-$x1, $pos[1]-$y1)
;~ 		GUICtrlSetPos($Lab, $x1+1, $y1+1, $pos[0]-1, $pos[1]-1)
	EndIf

	; ��������� �������
	If _WinAPI_GetAsyncKeyState(0x01) = True And $stage = 0 Then        ; ������ ����� ������ ���� ������ ���
		$x1 = $pos[0]
		$y1 = $pos[1]
		$stage = 1
		WinMove($Select, '', $x1, $y1, $pos[0]-$x1, $pos[1]-$y1)
		GUISetState(@SW_SHOWNOACTIVATE, $Select)
	EndIf
	If _WinAPI_GetAsyncKeyState(0x01) = False And $stage = 1 Then        ; �������� ����� ������ ����
		$x2 = $pos[0]
		$y2 = $pos[1]
		IniWrite($sPath_ini, "coord", "x1", $x1)
		IniWrite($sPath_ini, "coord", "y1", $y1)
		IniWrite($sPath_ini, "coord", "x2", $x2)
		IniWrite($sPath_ini, "coord", "y2", $y2)
		IniWrite($sPath_ini, "coord", "state", '1')
		ClipPut ($x1 & ',' & $y1 & ',' & $x2 & ',' & $y2)
		Quit()
	EndIf
	If _WinAPI_GetAsyncKeyState(0x02) = True Then Quit() ; ������ ������ ������ ���� - �����
Until Not Sleep(10)

Func Pause()
	$draw = Not $draw
	If $draw Then
		GUISetState(@SW_SHOW, $Horz)
		GUISetState(@SW_SHOW, $Vert)
        GUISetState(@SW_SHOW, $Select)
	Else
		GUISetState(@SW_HIDE, $Horz)
		GUISetState(@SW_HIDE, $Vert)
        GUISetState(@SW_HIDE, $Select)
	EndIf
EndFunc

Func Quit()
	Exit
EndFunc


;~ #include <Misc.au3>
;~ If _IsPressed('01') Then ToolTip('���' & @LF & MouseGetPos(0) & 'x' & MouseGetPos(1), 0, 0)
;~ If _IsPressed('02') Then ToolTip('���' & @LF & MouseGetPos(0) & 'x' & MouseGetPos(1), 0, 0)